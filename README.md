# Sold

Simple One-Line DynDNS
======================

Uses SSH and Python to update TinyDNS / DJBDNS Server

JJW Summer-Fall 2021


Requirements
============

- Linux client located where you want the DynDNS (Dynamic DNS) to resolve
- ssh and keychain installed and configured on client
- djbdns / tinydns-based DNS server
- go.sh script on djbdns server, which does a "make" on the djbdns database


Installation
============

- clone this repo to your local client
- symlink etc/cron.d/sold-hourly and usr/sbin/sendmail into their respective places
- change the hostname (h=...) and dns server names in sold.hourly to match yours
- ensure etc/cron.d/sold-hourly is owned by root, and is not group/other writeable
- ensure your public key(s) are installed on the remote DNS server (see ssh-copyid), and you can login successfully
- ensure your local private keys are installed in your keychain, and you have already entered your passphrase once, and logged in


Configuration
=============

- IP addresses are added / updated into a file called dynamic.realtime.domainlist.txt on the DNS server, in the directory ~/domains
- You must ensure this is properly added how you want it, into the djbdns "data" file, before the "make"
- This is typically done in the go.sh script on the server


Optional
========

- uncomment cron line from /etc/rsyslog.d/50-default.conf and sudo service rsyslog restart
  - meta-output will then appear in /var/log/cron as well, by itself
- When testing, replace initial 0 in sold-cron with *, to do every minute instead of once per hour


Output
======

- /var/log/syslog
- /var/log/cron.log (Optional, see above)
- ~/mail.log, in mail file (Berkeley MBox) format - see https://www.loc.gov/preservation/digital/formats/fdd/fdd000383.shtml
- or /var/spool/mail/$USER, depending on sendmail configuration

Example, run locally:

    (env) joe@studio-tricia:~/sold$ export h=`hostname`.jw.vc && ssh ns2.libregroup.net 'cd domains && ip=$(echo $SSH_CLIENT | cut -f1 -d" ") && h=studio-tricia.jw.vc && cur=$(grep $h dynamic.realtime.domainlist.txt) && ((grep $ip <<< $cur) && echo Found || (sed -ri".bak" s/^$h.*/"$h $ip"/ dynamic.realtime.domainlist.txt && echo Updated && sudo sh ./go.sh)) || (printf "\n$h $ip" >> dynamic.realtime.domainlist.txt && echo Added && sudo sh ./go.sh)'
    Updated
    /usr/local/bin/tinydns-data
    DOMAINS MADE OK!
    (env) joe@studio-tricia:~/sold$ 


Questions?







